package pacman;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.*;

/**
 * Juego del PacMan. Trabajo de clase.
 *
 * @version 06/01/2019
 * @author DaPeLle, Carlos, Millán.
 */
public class Juego extends JFrame {

    // Estado de la partida.
    private boolean pausarHilos;

    // Control de velocidad.
    private static final long VELOCIDAD_FANTASMA = 200;
    private static final long VELOCIDAD_PACMAN = 140;

    // Direcciones de movimiento de pacman y fantasmas.
    public static final byte QUIETO = 0;
    public static final byte ARRIBA = 1;
    public static final byte ABAJO = 2;
    public static final byte IZQUIERDA = 3;
    public static final byte DERECHA = 4;

    // Constantes usadas para el metodo comprobar casilla, este dice que accion o acciones se van a realizar.
    private static final byte PUEDES_MOVERTE = 0;
    private static final byte CHOQUE_PARED = 1;
    private static final byte CHOQUE_IGUALES = 2;
    private static final byte ELIMINADO = 3;

    // Elementos tablero.
    private static final byte PARED = 0;
    private static final byte VACIO = 1;
    public static final byte PACMAN = 2;
    public static final byte FANTASMA = 3;

    // Colores.
    private final Color COLOR_FONDO_ESCENARIO = Color.black;
    private final Color COLOR_FONDO_ESCENARIO_FIN = Color.blue;
    private final Color COLOR_PANEL_INFORMATIVO = Color.black;
    private final Color COLOR_LETRA = Color.white;

    // Elementos swing.
    private final JPanel panelCentral, panelSur, panelVidas;
    private final JLabel lblVida;
    private final JLabel[] coleccionImgVidas;
    private final String tituloVentana = "PacMan";

    // Imágenes.
    private ImageIcon pacmanImagen;
    private final ImageIcon imgCamino = new ImageIcon(getClass().getResource("/recursos/suelo.png"));
    private final ImageIcon coleccionImgFantasmas[] = {
        new ImageIcon(getClass().getResource("/recursos/fantasma1.png")),
        new ImageIcon(getClass().getResource("/recursos/fantasma2.png")),
        new ImageIcon(getClass().getResource("/recursos/fantasma3.png")),
        new ImageIcon(getClass().getResource("/recursos/fantasma4.png")),
        new ImageIcon(getClass().getResource("/recursos/fantasma5.png")),
        new ImageIcon(getClass().getResource("/recursos/fantasma6.png"))};
    private final ImageIcon imgPacManArriba = new ImageIcon(getClass().getResource("/recursos/pacman_arriba.png"));
    private final ImageIcon imgPacManAbajo = new ImageIcon(getClass().getResource("/recursos/pacman_abajo.png"));
    private final ImageIcon imgPacManIzq = new ImageIcon(getClass().getResource("/recursos/pacman_izq.png"));
    private final ImageIcon imgPacManDcha = new ImageIcon(getClass().getResource("/recursos/pacman_dcha.png"));
    private final ImageIcon imgPacManArribaCerrada = new ImageIcon(getClass().getResource("/recursos/pacman_arriba_cerrado.png"));
    private final ImageIcon imgPacManAbajoCerrada = new ImageIcon(getClass().getResource("/recursos/pacman_abajo_cerrado.png"));
    private final ImageIcon imgPacManIzqCerrada = new ImageIcon(getClass().getResource("/recursos/pacman_izq_cerrado.png"));
    private final ImageIcon imgPacManDchaCerrada = new ImageIcon(getClass().getResource("/recursos/pacman_dcha_cerrado.png"));
    private final ImageIcon imgPacManMuerto = new ImageIcon(getClass().getResource("/recursos/pacman_muerto.png"));
    private final ImageIcon imgPared = new ImageIcon(getClass().getResource("/recursos/pared.png"));
    private final ImageIcon imgVidaPacman = new ImageIcon(getClass().getResource("/recursos/pacman_vida.png"));
    private final ImageIcon imgVidaPacmanGastada = new ImageIcon(getClass().getResource("/recursos/pacman_vida_gastada.png"));

    // Otros.
    private final ArrayList<Personaje> coleccionFantasmas;
    private Personaje pacman;
    private byte vidasPacMan;
    private final byte vidasMaxPacMan = 5;
    private boolean bocaAbierta;
    private final ThreadGroup grupoHilos;
    private Random rand;

    // Escenarios.
    private final int ESCENARIO_ACTUAL[][];

    // Orden Y, X
    // 0 pared, 1 vacio, 2 PacMan, 3 fantasma.
    private final static int ESCENARIO_ORIGINAL[][] = {
        {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0},
        {0, 2, 1, 1, 1, 1, 0, 1, 1, 1, 1, 3, 0},
        {0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0},
        {0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0},
        {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},
        {0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0},
        {0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0},
        {0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 1, 0},
        {0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0},
        {0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0},
        {0, 1, 3, 1, 1, 1, 0, 1, 1, 1, 1, 3, 0},
        {0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0}};

    private final int FILAS = ESCENARIO_ORIGINAL.length;
    private final int COLUMNAS = ESCENARIO_ORIGINAL[0].length;

    /**
     * Constructor de la clase Juego. Inicializa todos los elementos para
     * conformar el juego.
     */
    public Juego() {
        // Definir estado de la partida.
        pausarHilos = false;

        // Crear un nuevo escenario.
        ESCENARIO_ACTUAL = new int[FILAS][COLUMNAS];

        // Crear un nuevo grupo de hilos.
        this.grupoHilos = new ThreadGroup("Objetos");

        // Definir vidas del pacman.
        vidasPacMan = vidasMaxPacMan;
        coleccionImgVidas = new JLabel[25];

        // Dejar al pacman con la boca abierta.
        bocaAbierta = true;

        // Crear elementos.
        this.coleccionFantasmas = new ArrayList<Personaje>();
        this.panelCentral = new JPanel();
        this.panelSur = new JPanel();
        this.panelVidas = new JPanel();
        this.lblVida = new JLabel("Vidas restantes");

        // Establecer layouts.
        panelCentral.setLayout(new GridLayout(FILAS, COLUMNAS));
        this.getContentPane().setLayout(new BorderLayout());
        panelSur.setLayout(new GridLayout(2, 1, 5, 5));
        panelVidas.setLayout(new GridLayout(1, coleccionImgVidas.length));

        // Añadir elementos a cada panel.
        this.getContentPane().add(panelCentral, BorderLayout.CENTER);
        this.getContentPane().add(panelSur, BorderLayout.SOUTH);
        panelSur.add(lblVida);
        panelSur.add(panelVidas);

        // Definir estilos.
        setIconImage(Toolkit.getDefaultToolkit().getImage(Juego.class.getResource("/recursos/icono.png")));
        panelCentral.setBackground(COLOR_FONDO_ESCENARIO);
        panelSur.setBackground(COLOR_PANEL_INFORMATIVO);
        panelVidas.setBackground(COLOR_PANEL_INFORMATIVO);
        lblVida.setForeground(COLOR_LETRA);
        panelSur.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        // Añadir vidas al panel de vidas.
        for (int i = 0; i < coleccionImgVidas.length; i++) {
            coleccionImgVidas[i] = new JLabel();
            panelVidas.add(coleccionImgVidas[i]);
        }

        // Crear instancia de random.
        // Para cuando los fantasmas necesitan generar una nueva dirección.
        this.rand = new Random();

        // Crear el escenario del juego.
        this.crearEscenario();

        // Cargar eventos de teclado.
        this.eventosTeclado();

        // Otros.
        this.setTitle("PacMan");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    // Esto es lo que sucederá al pulsar alguna tecla.
    private void eventosTeclado() {

        // Control de eventos de teclado para pacman.
        this.addKeyListener(new KeyListener() {

            // No se usa.
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (!pausarHilos) {
                    ArrayList<Byte> direcciones = getPosicionesValidas(pacman.getY(), pacman.getX(), pacman.getDireccion(), PACMAN);

                    switch (e.getExtendedKeyCode()) {
                        // DERECHA (D o flecha)
                        case 68:
                        case 39:
                            pacman.setDireccion(direcciones.contains(DERECHA) ? DERECHA : pacman.getDireccion());
                            break;

                        // IZQUIERDA (A o flecha)
                        case 65:
                        case 37:
                            pacman.setDireccion(direcciones.contains(IZQUIERDA) ? IZQUIERDA : pacman.getDireccion());
                            break;

                        // ABAJO (S o flecha)
                        case 83:
                        case 40:
                            pacman.setDireccion(direcciones.contains(ABAJO) ? ABAJO : pacman.getDireccion());
                            break;

                        // ARRIBA (W o flecha)
                        case 87:
                        case 38:
                            pacman.setDireccion(direcciones.contains(ARRIBA) ? ARRIBA : pacman.getDireccion());
                            break;

                        // Pulsar la letra M, esto muestra por consola el mapa logico
                        case 77:
                            mostrarEscenarios();
                            break;
                        default:
                            break;
                    }
                }
                // PAUSE (P)
                if (e.getExtendedKeyCode() == 80) {
                    Juego.this.pausarHilos = !Juego.this.pausarHilos;
                    if (!Juego.this.pausarHilos) {
                        despertarHilos();
                        Juego.this.setTitle(tituloVentana);
                    } else {
                        Juego.this.setTitle(tituloVentana + " - PAUSADO");
                    }
                }
            }

            // No se usa.
            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

    }

    // Usado SOLO para pausar el juego. Llama a todos los hilos.
    private synchronized void despertarHilos() {
        notifyAll();
    }

    /**
     * Cambia la imagen del pacman en función de su dirección. Es llamado desde
     * pacman.
     *
     * @param direccion
     */
    public synchronized void definirImagenPacman(byte direccion) {
        bocaAbierta = !bocaAbierta;

        switch (direccion) {
            case ARRIBA:
                pacmanImagen = bocaAbierta ? imgPacManArriba : imgPacManArribaCerrada;
                break;
            case ABAJO:
                pacmanImagen = bocaAbierta ? imgPacManAbajo : imgPacManAbajoCerrada;
                break;
            case IZQUIERDA:
                pacmanImagen = bocaAbierta ? imgPacManIzq : imgPacManIzqCerrada;
                break;
            case DERECHA:
                pacmanImagen = bocaAbierta ? imgPacManDcha : imgPacManDchaCerrada;
                break;
        }
    }

    // Crea y muestra el escenario.
    private void crearEscenario() {

        // Antes de nada hay que cargar el escenario original en el escenario actual.
        cargarEscenarioOriginal();

        JLabel etiqueta = null;
        int xindice = 0;
        pacmanImagen = imgPacManDcha;

        // Crear escenario.
        for (int y = 0; y < FILAS; y++) {
            for (int x = 0; x < COLUMNAS; x++) {
                if (ESCENARIO_ACTUAL[y][x] == PARED) {
                    etiqueta = new JLabel(imgPared);

                } else if (ESCENARIO_ACTUAL[y][x] == VACIO) {
                    etiqueta = new JLabel(imgCamino);

                } else if (ESCENARIO_ACTUAL[y][x] == PACMAN) {
                    etiqueta = new JLabel(pacmanImagen);
                    pacman = new Personaje(y, x, this, 0, PACMAN, VELOCIDAD_PACMAN);
                    Thread hiloPacman = new Thread(grupoHilos, pacman);
                    hiloPacman.start();

                } else if (ESCENARIO_ACTUAL[y][x] == FANTASMA) {
                    etiqueta = new JLabel(getImagenInicialFantasma());
                    Personaje f = new Personaje(y, x, this, coleccionFantasmas.size(), FANTASMA, VELOCIDAD_FANTASMA);
                    coleccionFantasmas.add(f); // Añado el fantasma a una colección.
                }
                // Le pongo a la etiqueta un nombre.
                etiqueta.setName(Integer.toString(y) + "_" + Integer.toString(x));

                panelCentral.add(etiqueta, xindice++);
            }
        }

        // Arrancar todos los fantasmas.
        for (Personaje f : coleccionFantasmas) {
            Thread hf = new Thread(grupoHilos, f);
            hf.start();
        }

        setResizable(false);
        getContentPane().setBackground(COLOR_FONDO_ESCENARIO);

        pintarVidasPacMan();

        pack();
    }

    // Recarga de nuevo todas las imagenes siguiendo con el mapa lógico.
    private void refrescar() {
        this.repaint();
        JLabel etiqueta = null;
        int indiceGrid = 0;
        int objeto = 0;

        for (int y = 0; y < FILAS; y++) {
            for (int x = 0; x < COLUMNAS; x++) {
                objeto = ESCENARIO_ACTUAL[y][x];
                if (objeto == PARED) {
                    indiceGrid++;
                    continue;
                } else if (objeto == VACIO) {
                    etiqueta = (JLabel) panelCentral.getComponent(indiceGrid);
                    etiqueta.setIcon(imgCamino);
                } else if (objeto == PACMAN) {
                    etiqueta = (JLabel) panelCentral.getComponent(indiceGrid);
                    etiqueta.setIcon(pacmanImagen);
                } else if (objeto == FANTASMA) {
                    etiqueta = (JLabel) panelCentral.getComponent(indiceGrid);
                    int i = getImagenFantasma(y, x);
                    if (i > coleccionImgFantasmas.length - 1) {
                        etiqueta.setIcon(coleccionImgFantasmas[0]);
                    } else {
                        etiqueta.setIcon(coleccionImgFantasmas[i]);
                    }
                }
                indiceGrid++;
            }
        }
        this.validate();
    }

    // Obtiene el índice de imagen correspondiente al fantasma que está en una determinada coordenada.
    private int getImagenFantasma(int y, int x) {
        for (Personaje f : coleccionFantasmas) {
            if (f.getY() == y && f.getX() == x) {
                return f.getId();
            }
        }
        return 0;
    }

    // Obtiene la imagen que le corresponde al nuevo fantasma.
    private ImageIcon getImagenInicialFantasma() {
        if (coleccionFantasmas.size() >= coleccionImgFantasmas.length) {
            return coleccionImgFantasmas[0];
        } else {
            return coleccionImgFantasmas[coleccionFantasmas.size()];
        }

    }

    // Pinta las vidas del PacMan.
    private void pintarVidasPacMan() {
        for (int i = 0; i < vidasPacMan; i++) {
            coleccionImgVidas[i].setIcon(imgVidaPacman);
        }
        for (int i = vidasPacMan; i < coleccionImgVidas.length; i++) {
            coleccionImgVidas[i].setIcon(imgVidaPacmanGastada);
        }
    }

    // Comprueba la casilla a la que se dirige el objeto y le dice que hacer.
    // Devuelve: int (orden, coordenada Y, coordenada X)
    // 1º Resultado - 0 se puede mover, 1 choque pared, 2 choque iguales, 3 comecocos eliminado.
    // 2º y 3º Coordenada Y, X.
    private int[] comprobarCasilla(int y, int x, int personaje, byte direccion) {
        int[] resultado = {PUEDES_MOVERTE, 0, 0};
        int yFutura = y, xFutura = x, objeto;

        // Obtiene la posicion siguiente.
        // Esto es lo que permite salir de un límite y aparecer en el otro.
        switch (direccion) {
            case ARRIBA:
                yFutura = y - 1 < 0 ? FILAS - 1 : y - 1;
                break;
            case ABAJO:
                yFutura = y + 1 >= FILAS ? 0 : y + 1;
                break;
            case IZQUIERDA:
                xFutura = x - 1 < 0 ? COLUMNAS - 1 : x - 1;
                break;
            case DERECHA:
                xFutura = x + 1 >= COLUMNAS ? 0 : x + 1;
                break;
            default:
        }

        // Obtener el objeto con el cual se va a topar. Predicción.
        objeto = ESCENARIO_ACTUAL[yFutura][xFutura];

        // Interpretar los resultados.
        if (objeto == PARED) {
            resultado[0] = CHOQUE_PARED;
        } else if (objeto == VACIO) {
            resultado[0] = PUEDES_MOVERTE;
        } else {
            switch (personaje) {
                case PACMAN:
                    if (objeto == FANTASMA) {
                        resultado[0] = ELIMINADO;
                    }
                    break;
                case FANTASMA:
                    if (objeto == FANTASMA) {
                        resultado[0] = CHOQUE_IGUALES;
                    } else if (objeto == PACMAN) {
                        resultado[0] = ELIMINADO;
                    }
                    break;
            }
        }
        resultado[1] = xFutura;
        resultado[2] = yFutura;

        return resultado;
    }

    // Obtiene un ArrayList con las posiciones válidas.
    private ArrayList<Byte> getPosicionesValidas(int y, int x, byte direccion, int personaje) {

        boolean[] direcciones = new boolean[4];
        byte dc = direccionContraria(direccion);
        ArrayList<Byte> coleccionDirecciones = new ArrayList<Byte>();

        // Obtener objetos adyacentes
        direcciones[0] = !hayObstaculo(ESCENARIO_ACTUAL[y - 1 < 0 ? FILAS - 1 : y - 1][x], personaje); // Arriba
        direcciones[1] = !hayObstaculo(ESCENARIO_ACTUAL[y + 1 >= FILAS ? 0 : y + 1][x], personaje); // Abajo
        direcciones[2] = !hayObstaculo(ESCENARIO_ACTUAL[y][x - 1 < 0 ? COLUMNAS - 1 : x - 1], personaje); // Izquierda
        direcciones[3] = !hayObstaculo(ESCENARIO_ACTUAL[y][x + 1 >= COLUMNAS ? 0 : x + 1], personaje); // Derecha

        if (personaje == FANTASMA) {
            for (byte i = 0; i < 4; i++) {
                if (i + 1 == dc) {
                    direcciones[i] = false;
                    break;
                }
            }
        }

        // Crea un array con las direcciones posibles.
        for (byte i = 0; i < 4; i++) {
            if (direcciones[i]) {
                coleccionDirecciones.add((byte) (i + 1));
            }
        }
        return coleccionDirecciones;
    }

    // Obtiene la direccion contraria a la direccion dada.
    private byte direccionContraria(byte d) {
        if (d == ARRIBA) {
            return ABAJO;
        } else if (d == ABAJO) {
            return ARRIBA;
        } else if (d == DERECHA) {
            return IZQUIERDA;
        } else {
            return DERECHA;
        }
    }

    // Comprueba si el objeto pasado es un obstáculo para el personaje.
    private boolean hayObstaculo(int objeto, int personaje) {
        if (personaje == PACMAN) {
            return objeto == PARED;
        } else {
            return objeto == PARED || objeto == FANTASMA;
        }
    }

    /**
     * Mueve un personaje en el mapa lógico y actualiza las coordenadas del
     * objeto.
     *
     * @param y Coordenada Y del personaje.
     * @param x Coordenada X del personaje.
     * @param personaje Fantasma o PacMan.
     * @param direccion Dirección hacia la que va.
     * @param id Identificador del fantasma. Al PacMan le da igual.
     */
    protected synchronized void moverPersonaje(int y, int x, int personaje, byte direccion, int id) {
        if (!pausarHilos) {

            int[] accionCoordenadas = comprobarCasilla(y, x, personaje, direccion);
            int xPrima = accionCoordenadas[1];
            int yPrima = accionCoordenadas[2];

            switch (accionCoordenadas[0]) {
                case PUEDES_MOVERTE:
                    ESCENARIO_ACTUAL[y][x] = VACIO;
                    ESCENARIO_ACTUAL[yPrima][xPrima] = personaje;

                    if (personaje == PACMAN) {
                        this.pacman.setYX(yPrima, xPrima);
                        definirImagenPacman(direccion);
                    }
                    if (personaje == FANTASMA) {
                        coleccionFantasmas.get(id).setYX(yPrima, xPrima);
                    }
                    break;
                case CHOQUE_IGUALES:
                case CHOQUE_PARED:
                    if (personaje == FANTASMA) { // REBOTE

                        Personaje fantasma = coleccionFantasmas.get(id);

                        ArrayList<Byte> coleccionDireccionesPosibles = getPosicionesValidas(fantasma.getY(), fantasma.getX(), fantasma.getDireccion(), FANTASMA);

                        int i = coleccionDireccionesPosibles.size();
                        if (i == 0) { // EXCEPCION - Este caso es en el que un fantasma choca contra otro y solo puede volver por donde ha venido.
                            fantasma.setDireccion(direccionContraria(direccion));
                        } else {
                            int valor = this.getValorAleatorio(i);
                            fantasma.setDireccion(coleccionDireccionesPosibles.get(valor));
                        }
                    }
                    if (personaje == PACMAN) {
                        this.pacman.setDireccion(QUIETO);
                    }
                    break;
                case ELIMINADO:
                    if (personaje == PACMAN || personaje == FANTASMA) {

                        // Fin de partida
                        pausarHilos = true;

                        // Dibujar escenario fin con pacman acabado
                        ESCENARIO_ACTUAL[y][x] = VACIO;
                        ESCENARIO_ACTUAL[yPrima][xPrima] = 2;
                        this.pacmanImagen = imgPacManMuerto;

                        refrescar();

                        // Reiniciar objetos
                        this.pacman.matarHilo();
                        for (Personaje f : coleccionFantasmas) {
                            f.matarHilo();
                        }

                        // Comprobar si le quedan vidas o no
                        if (vidasPacMan > 0) {
                            vidasPacMan--;
                            pintarVidasPacMan();
                            try {
                                for (int i = 0; i < 3; i++) {
                                    this.wait(200);
                                    panelCentral.setBackground(COLOR_FONDO_ESCENARIO_FIN);
                                    this.wait(200);
                                    panelCentral.setBackground(COLOR_FONDO_ESCENARIO);
                                }
                                crearHiloReset();

                            } catch (InterruptedException ex) {
                                System.out.println("ERROR - Escenario " + ex);
                            }
                        } else {
                            panelCentral.setBackground(COLOR_FONDO_ESCENARIO_FIN);
                            int i = JOptionPane.showConfirmDialog(this, "¡Ya no te quedan más vidas!\n¿Deseas empezar de nuevo?", "GAME OVER!", JOptionPane.YES_NO_OPTION);
                            if (i == 0) {
                                vidasPacMan = vidasMaxPacMan;
                                pintarVidasPacMan();
                                crearHiloReset();
                            } else {
                                System.exit(0);
                            }
                        }
                        return; // Para que no refresque
                    }
                default:
            }
            refrescar();
        }
    }

    /**
     * Comprueba que solo queda un hilo con vida. En este caso el hilo que
     * reinicia todo (hiloReset).
     *
     * @return true si hay hilos vivos, false si no los hay.
     */
    public boolean quedaMasDeUnHiloVivo() {
        return grupoHilos.activeCount() > 1;
    }

    /**
     * Crea un hilo el cual llama al método nuevoIntento, asegurandose antes de
     * que los otros hilos no existen.
     */
    protected void crearHiloReset() {
        new Thread(new HiloReset(this)).start();
    }

    /**
     * Si el personaje muere este vuelve a cargar el mapa y los hilos.
     */
    protected void nuevoIntento() {

        // Valores por defecto.
        pausarHilos = false;
        bocaAbierta = true;
        panelCentral.setBackground(COLOR_FONDO_ESCENARIO);

        // Sobreescribir hilos.
        Thread hilo;
        hilo = new Thread(grupoHilos, pacman);
        hilo.start();

        for (Personaje f : coleccionFantasmas) {
            hilo = new Thread(grupoHilos, f);
            hilo.start();
        }

        Juego.this.setTitle(tituloVentana);
        pacmanImagen = imgPacManDcha;
        cargarEscenarioOriginal();
    }

    // Crea una copia idéntica del escenario original en el escenario actual.
    private void cargarEscenarioOriginal() {
        for (int y = 0; y < FILAS; y++) {
            for (int x = 0; x < COLUMNAS; x++) {
                ESCENARIO_ACTUAL[y][x] = ESCENARIO_ORIGINAL[y][x];
            }
        }
    }

    // Al pulsar la letra M del teclado se muestra una imagen del tablero lógico. Solo consola.
    private void mostrarEscenarios() {
        System.out.println("Instantánea actual del mapa lógico:");
        for (int y = 0; y < FILAS; y++) {
            for (int x = 0; x < COLUMNAS; x++) {
                System.out.print(ESCENARIO_ACTUAL[y][x] + " ");
            }
            System.out.println("");
        }
    }

    /**
     * Obtiene un valor aleatorio que va de 0 al valorLimite pasado por
     * parámetro.
     *
     * @param valorLimite Es el valor máximo que va a generar.
     * @return byte Valor generado.
     */
    public synchronized byte getValorAleatorio(int valorLimite) {
        if (valorLimite > Byte.MAX_VALUE || valorLimite < Byte.MIN_VALUE) {
            return 0;
        } else {
            return (byte) rand.nextInt(valorLimite);
        }
    }

    // Método principal.
    public static void main(String[] args) {
        new Juego();
    }
}
